import Vue from 'vue';
import Vuesax from 'vuesax';

import 'vuesax/dist/vuesax.css';
import 'material-icons/iconfont/material-icons.css';

Vue.use(Vuesax, {
  theme: {
    colors: {
      primary: '#3E89F9',
      secondary: '#c83492',
      success: '#00C703',
      warning: '#E2A230',
      danger: '#E50A0A',
      black: '#121212',
      white: '#ffffff',
      'grey-001': '#aaaaaa',
      'grey-002': '#dedede',
      'grey-003': '#ebedf8',
      'grey-004': '#f5f6fE'
    }
  }
});
