import Vue from 'vue';
import axios from 'axios';
import store from '../store';
import router from '../router';
axios.defaults.baseURL = process.env.VUE_APP_BASE_URL ?? '//api.local/';
axios.defaults.headers.common.fromweb = 1;
const axiosInstance = axios.create();

axiosInstance.interceptors.request.use(
  config => {
    if (store.getters['Auth/isAuthenticated']) {
      config.headers['Authorization'] = `Bearer ${store.getters['Auth/token']}`;
    }
    return config;
  },
  error => Promise.reject(error)
);

axiosInstance.interceptors.response.use(
  response => response,
  error => {
    const errorResponse = error?.response;
    const originalRequest = error.config;

    if (errorResponse?.status === 401 && originalRequest.url === 'auth/refresh') {
      router.push({ name: 'Auth', replace: true }).catch(() => {});
    }

    if (!errorResponse?.data?.disableNotification && !axios.isCancel(error) && errorResponse?.status !== 401) {
      store.dispatch('General/showErrorReqNotification', error);
    }

    if (errorResponse?.status === 401 && !originalRequest._retry && errorResponse?.config?.url !== 'auth/login') {
      originalRequest._retry = true;
      return axios
        .post('auth/token/refresh', { token: store.getters['Auth/refreshToken'] })
        .then(
          ({
            data: {
              results: { token, refreshToken, user }
            }
          }) => {
            store.commit('Auth/SET_AUTH', { token, refreshToken, user });
            axios.defaults.headers.common['Authorization'] = `Bearer ${store.getters['Auth/token']}`;
            return axiosInstance(originalRequest);
          }
        )
        .catch(() => {
          store.commit('Auth/LOGOUT');
          router.push({ name: 'Auth', replace: true }).catch(() => {});
        });
    }

    return Promise.reject(error);
  }
);

Plugin.install = function(Vue) {
  window.axios = axiosInstance;
  Object.defineProperties(Vue.prototype, {
    axios: {
      get() {
        return axiosInstance;
      }
    }
  });
};

Vue.use(Plugin);

export default Plugin;
